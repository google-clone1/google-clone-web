import { Component, OnInit, ViewChild } from '@angular/core';

import { ApexChart, ApexNonAxisChartSeries, ApexResponsive, ChartComponent } from 'ng-apexcharts';
import { Router } from '@angular/router';
import * as _ from 'lodash';
import { ApiService } from '../helper/api.service';
import { GeneralService } from '../helper/general.service';


export type ChartOptions = {
  series: ApexNonAxisChartSeries | any;
  chart: ApexChart | any;
  responsive: ApexResponsive[] | any;
  labels: any;
};

@Component({
  selector: 'app-summary-page',
  templateUrl: './summary-page.component.html',
  styleUrls: ['./summary-page.component.scss']
})
export class SummaryPageComponent implements OnInit {

  @ViewChild("chart") chart!: ChartComponent;


  totalSubmit = 0

  public chartOptionsList: Partial<ChartOptions>[] = []


  constructor(public generalService: GeneralService,
    public router: Router,
    public apiservice: ApiService) {
    for (let index = 0; index < 4; index++) {
      this.chartOptionsList[index] = _.cloneDeep({
        series: [0, 0, 0, 0, 0],
        chart: {
          width: 380,
          type: "pie"
        },
        labels: ["A", "B", "C", "D", "E"],
        responsive: [
          {
            breakpoint: 480,
            options: {
              chart: {
                width: 200
              },
              legend: {
                position: "bottom"
              }
            }
          }
        ]
      })
    }

    this.getResult()
  }

  ngOnInit(): void {
  }

  getResult() {
    this.apiservice.getResult().subscribe(res => {
      this.totalSubmit = _.get(res, 'totalRequest', 0)
      const graphList = _.get(res, 'graphList', [])
      this.chartOptionsList.forEach(async (element, index) => {
        element.series = _.get(graphList[index], 'series', [])
        element.labels = _.get(graphList[index], 'labels', [])
      });
    })
  }

}
