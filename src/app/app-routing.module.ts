import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormPageComponent } from './form-page/form-page.component';
import { ResultPageComponent } from './result-page/result-page.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { SummaryPageComponent } from './summary-page/summary-page.component';

const routes: Routes = [
  {
    path: 'sign-in',
    component: SignInComponent
  },
  {
    path: 'form-page',
    component: FormPageComponent
  },
  {
    path: 'result-page',
    component: ResultPageComponent
  },
  {
    path: 'summary-page',
    component: SummaryPageComponent
  },
  { path: '', component: SignInComponent },
  { path: '**', redirectTo: 'sign-in' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
