import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import * as _ from 'lodash';

@Injectable()
export class ApiService {

  dogImageURL = 'https://dog.ceo/api/breeds/image/random/'
  nameListURL = 'https://randommer.io/Name'

  mainUrlBE = environment.mainAPI

  constructor(private httpClient: HttpClient) { }

  public getDogImage(numberOfImage: string) {
    return this.httpClient.get(
      this.dogImageURL + numberOfImage
    );
  }

  public getNameList(countName: string) {
    const formData = new FormData()
    formData.append('type', 'firstname')
    formData.append('number', countName)
    formData.append('X-Requested-With', 'XMLHttpRequest')

    return this.httpClient.post(
      this.nameListURL, formData
    );
  }

  public submitForm(body: string[]) {
    return this.httpClient.post(
      this.mainUrlBE + '/form', body
    );
  }


  public getResult() {
    return this.httpClient.get(
      this.mainUrlBE + '/result'
    );
  }



}
