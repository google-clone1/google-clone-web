import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import * as _ from 'lodash';
import { ToastrService } from 'ngx-toastr';
import { PopupComponent } from '../component/popup/popup.component';
import { ApiService } from '../helper/api.service';
import { GeneralService } from '../helper/general.service';

@Component({
  selector: 'app-form-page',
  templateUrl: './form-page.component.html',
  styleUrls: ['./form-page.component.scss']
})
export class FormPageComponent implements OnInit {

  imageUrlList: string[] = []
  isLoading = false
  isNameListLoading = false

  nameList: string[] = []

  s_ANS: any = Array.from({ length: 5 }).fill(undefined);
  ANS1List = ['😊', '😔', '😠', '😨', '😲', '🤢', '😆', '❤️', '😕', '😤']
  httpCodeList = ['200', '300', '400', '500']

  email: string | null = null

  constructor(public dialog: MatDialog,
    private apiService: ApiService,
    public router: Router,
    public generalService: GeneralService,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    this.isLoading = true
    this.apiService.getDogImage('10').subscribe((res: any) => {
      this.imageUrlList = res.message
      this.isLoading = false
    })
    this.fetchAndAppenNameToList()
    this.s_ANS[2] = new FormControl(new Date())
    this.email = localStorage.getItem("email");
  }


  fetchAndAppenNameToList() {
    this.isNameListLoading = true
    this.apiService.getNameList('50').subscribe((res: any) => {
      this.isNameListLoading = false
      this.nameList.push(...res)
    })
  }

  clearANS(index: number) {
    if (2) {
      this.s_ANS[index] = new FormControl(new Date())
    } else {
      this.s_ANS[index] = undefined
    }
  }



  openImagePopup(imageUrl: string) {
    const dialogRef = this.dialog.open(PopupComponent, {
      data: {
        imageUrl
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('dialog closed');
    });
  }

  clearAll() {
    for (let index = 0; index < this.s_ANS.length; index++) {
      this.clearANS(index)
    }
  }

  submit() {
    let body = _.cloneDeep(this.s_ANS)
    if (body[2]) {
      body[2] = body[2].value.toLocaleDateString('en-GB');
    }
    body.shift();
    this.apiService.submitForm(body).subscribe(res => {
      this.toastr.success(_.get(res, 'message'));
      this.router.navigate(['/result-page'])
    })

  }
}
