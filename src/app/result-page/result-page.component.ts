import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as _ from 'lodash';
import { GeneralService } from '../helper/general.service';


@Component({
  selector: 'app-result-page',
  templateUrl: './result-page.component.html',
  styleUrls: ['./result-page.component.scss']
})
export class ResultPageComponent implements OnInit {

  constructor(
    public generalService: GeneralService,
    public router: Router
  ) {
  }

  ngOnInit(): void {

  }
}