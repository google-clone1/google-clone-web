# System requirement

Node 18.12.1

Angular 14.2.10


Backend local port 4444 

Repo : https://gitlab.com/google-clone1/google-clone-api


Backend URL for test : https://kim-google.datahungry.dev/

# Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Example firebase

[https://form-clone-e9294.web.app](https://form-clone-e9294.web.app)
